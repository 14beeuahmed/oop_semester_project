#include <opencv2\core\core.hpp>
#include <opencv2\contrib\contrib.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\objdetect\objdetect.hpp>
#include <opencv2\opencv.hpp>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "FaceRec_Prog.h"

void FaceRec::CSVRead(const std::string& database){
	std::ifstream file(database);

	if (!file){
		std::string error = "No Valid input file";
		CV_Error(CV_StsBadArg, error);
	}
	std::string line, path, label;
	while (getline(file, line))
	{
		std::stringstream liness(line);
		getline(liness, path, ';');
		getline(liness, label);
		if (!path.empty() && !label.empty()){
			images.push_back(cv::imread(path, 0));
			labels.push_back(stoi(label));
		}
	}
	std::cout << "Number of the images is " << images.size() << std::endl;
	std::cout << "Number of the labes is " << labels.size() << std::endl;
}

void FaceRec::FisherFaceTrainer(const std::string& f_trainer)
{
	std::cout << "Fisher-Face Training Started." << std::endl;
	cv::Ptr<cv::FaceRecognizer> model = cv::createFisherFaceRecognizer();	
	model->train(images, labels);
	model->save(f_trainer);
	std::cout << "Finished Training. File Saved." << std::endl;
	cv::waitKey(10000);
}

void FaceRec::LBPHFaceTrainer(const std::string& l_trainer)
{
	std::cout << "Linear Binary Pattern Training Started." << std::endl;
	cv::Ptr<cv::FaceRecognizer> model = cv::createLBPHFaceRecognizer();
	model->train(images, labels);
	model->save(l_trainer);
	std::cout << "Finished Training. File Saved." << std::endl;
	cv::waitKey(10000);
}

void FaceRec::FaceRecognition(const std::string& cascade, const std::string& s_trainer, const std::string& t_image)
{
	std::cout << "Started Face Recognition." << std::endl;
	cv::Ptr<cv::FaceRecognizer>  model;
	if (s_trainer == "Fisher.yml")
	{
		model = cv::createFisherFaceRecognizer();
	}
	else if (s_trainer == "LBP.yml")
	{
		model = cv::createLBPHFaceRecognizer();
	}
	model->load(s_trainer);
	cv::Mat testSample = images[images.size() - 1];
	int testLabel = labels[labels.size() - 1];
	images.pop_back();
	labels.pop_back();
	int img_width = testSample.cols;
	int img_height = testSample.rows;
	cv::CascadeClassifier face_cascade;
	std::string classifier = cascade;
	if (!face_cascade.load(classifier))
	{
		std::cout << " Error loading file" << std::endl;
		exit(1);
	}

	std::vector<cv::Mat> frames;
	std::ifstream load(t_image);
	std::vector<std::string> path;
	if (load.is_open())
	{
		std::string frame;
		while (getline(load, frame))
		{
			if (!frame.empty())
			{
				frames.push_back(cv::imread(frame, 1));
				path.push_back(frame);
			}
			else
				std::cerr << "Error Reading Test File.";
		}
	}
	else
		std::cerr << "Error Opening Test File.";

	std::vector<cv::Rect> faces;
	cv::Mat graySacleFrame;
	std::vector<int> t_label (40,0);
	std::ofstream write ("Data_Output.csv");
	for (unsigned int itr = 0; itr <= frames.size()-1; itr++)
	{
		if (!frames[itr].empty())
		{
			cvtColor(frames[itr], graySacleFrame, CV_BGR2GRAY);
			face_cascade.detectMultiScale(graySacleFrame, faces, 1.1, 3, 0, cv::Size(90, 90));
			std::cout << faces.size() << " face(s) detected." << std::endl;
			//if (faces.size() == 0)
				//continue;

			int width = 0, height = 0;
			for (unsigned int i = 0; i < faces.size(); i++)
			{
				cv::Rect face_i = faces[i];
				cv::Mat face = graySacleFrame(face_i);
				cv::Mat face_resized;
				cv::resize(face, face_resized, cv::Size(img_width, img_height), 1.0, 1.0, cv::INTER_CUBIC);
				int label = -1; double confidence = 0;
				model->predict(face_resized, label, confidence);
				t_label[itr] = label;
				std::cout << "Confidence Level : " << confidence << std::endl;
			}
			faces.clear();
			if (write.is_open())
			{
				write << path[itr] << ";" << t_label[itr] <<"\n";
			}
		}
		else
			std::cerr << "Image file Empty!";
	}
	write.close();
	images.clear();
	labels.clear();
}