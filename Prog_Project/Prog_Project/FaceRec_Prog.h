#include <opencv2\core\core.hpp>
#include <opencv2\contrib\contrib.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\objdetect\objdetect.hpp>
#include <opencv2\opencv.hpp>

#include <vector>
#include <string>

class FaceRec
{
private:
	std::vector <cv::Mat> images;
	std::vector <int> labels;

public:
	void CSVRead(const std::string&);
	void FisherFaceTrainer(const std::string&);
	void LBPHFaceTrainer(const std::string&);
	void FaceRecognition(const std::string&, const std::string&, const std::string&);
};