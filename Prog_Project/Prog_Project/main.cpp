#include <opencv2\core\core.hpp>
#include <opencv2\contrib\contrib.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\objdetect\objdetect.hpp>
#include <opencv2\opencv.hpp>

#include <iostream>
#include <fstream>

#include "FaceRec_Prog.h"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
	int choice;
	FaceRec Project;
	while (1)
	{
		ifstream f_check("Fisher.yml");
		ifstream l_check("LBP.yml");
		if (f_check.is_open() && l_check.is_open())
		{
			f_check.close();
			l_check.close();
			try
			{
				Project.CSVRead("Data.csv"/*argv[1]*/);
			}
			catch (cv::Exception& e){
				cerr << "Error opening the file." << e.msg << endl;
				exit(1);
			}
			cout << "Select an Option.\n1) Use Fisher-Faces Method.\n2) Use Linear Binary Pattern.\n3)Exit\n";
			cin >> choice;
			if (choice == 1)
				Project.FaceRecognition("haarcascade_frontalface_default.xml", "Fisher.yml", "images.csv"/*argv[2]*/);
			else if (choice == 2)
				Project.FaceRecognition("lbpcascade_frontalface.xml", "LBP.yml", "images.csv"/*argv[2]*/);
			else if (choice == 3)
				break;
		}
		else if (!f_check.is_open() || !l_check.is_open())
		{
			try
			{
				Project.CSVRead("Data.csv"/*argv[1]*/);
			}
			catch (cv::Exception& e){
				cerr << "Error opening the file." << e.msg << endl;
				exit(1);
			}
			if (!f_check.good())
				Project.FisherFaceTrainer("Fisher.yml");
			if (!l_check.good())
				Project.LBPHFaceTrainer("LBP.yml");
		}
	}
	system("pause");
	return 0;
}